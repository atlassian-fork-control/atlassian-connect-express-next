# Atlassian Connect Express Next

** NOTE: This was experimental and is currently unsupported. There are some problems with server-side rendering of React dependencies (especially AtlasKit components).**

## Getting started

Make sure you've got `nodemon`, `babel-cli`, `ngrok` and `yarn` (optional) installed.

```bash
npm i -g nodemon babel-cli ngrok yarn
```

Clone this repo:

```bash
git clone https://bitbucket.org/atlassian/atlassian-connect-express-next.git
```

Install dependencies:

```bash
cd atlassian-connect-express-next
yarn
```

On a separate terminal, start a ngrok tunnel and then update `localBaseUrl` in `config.json` with the subdomain slug that you get.

```bash
ngrok http 3000
```

Start your application:
```bash
yarn run dev
```