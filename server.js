import ace from 'atlassian-connect-express';
import bodyParser from 'body-parser';
import express from 'express';
import next from 'next';
import { aceToNextPayload, aceEngineSetup } from './lib/aceAdaptors';
import setupRoutes from './routes';

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

app.prepare()
  .then(() => {
    const server = express();
    const addon = ace(server);

    // Configure Express middleware.
    aceEngineSetup(server);
    server.use(addon.middleware());
    server.use(aceToNextPayload);
    server.use(bodyParser.json());
    server.use(bodyParser.urlencoded({ extended: false }));

    // Define you app routes in routes.js
    setupRoutes(app, server, addon);

    server.listen(addon.config.port(), (err) => {
      if (err) throw err
      console.log(`> Ready on ${addon.config.localBaseUrl()}`);
    });
  });
